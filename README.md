## Description

This is my first project in Nest.js. It took approximately 8-10 hours to develop. I didn't have time to implement avatar upload, avatar deletion, and tests. And yes! I liked the framework, it's lightweight and concise.


## Microservices

- **notifications** - This service is responsible for sending notifications. Currently, only sending emails upon user creation is implemented.

Listens to the queue - notifications

- **users** - This service handles user data.

Endpoints:

1. GET /users/ - Get a list of users.
1. POST /users/ - Create a user.
1. GET /users/id/ - Get a user by ID.
1. DELETE /users/id/ - Delete a user.

> Also, there is RabbitMQ management on port: 15672. user: payever, password: payever


## Running monorepo


**If you have unpacked the zip file and the project doesn't run, please try cloning it from [https://gitlab.com/kapa.miki16/payever-services](https://gitlab.com/kapa.miki16/payever-services).**

```bash
$ docker compose up --build

# Old docker compose v
$ docker-compose up --build

```

## Send requests to endpoints

To test the endpoints, please import the **postman_collection.json** file into Postman.

