import { Module } from '@nestjs/common';
import { UsersController } from './users.controller';
import { UsersService } from './users.service';
import { ConfigModule } from "@nestjs/config";
import { DatabaseModule, RmqModule } from "@app/common";
import { MongooseModule } from "@nestjs/mongoose";
import { UserSchema } from "./schemas/users.schema";
import { HttpModule } from "@nestjs/axios";
import { NOTIFICATIONS_SERVICE } from "./constants/services";
import * as Joi from 'joi';


@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
      envFilePath: './apps/users/.env',
      validationSchema: Joi.object({
        DATABASE_URL: Joi.string().required(),
        REQRES_SERVICE_BASE_URL: Joi.string().required(),
      }),
    }),
    DatabaseModule,
    MongooseModule.forFeature([{name: 'User', schema: UserSchema}]),
    RmqModule.register({
      name: NOTIFICATIONS_SERVICE
    }),
    HttpModule.registerAsync({
      useFactory: () => ({
        timeout: 5000
      }),
    })
  ],
  controllers: [UsersController],
  providers: [UsersService],
})
export class UsersModule {}
