import { Inject, Injectable } from '@nestjs/common';
import { Model } from "mongoose";
import { User } from "./schemas/users.schema";
import { InjectModel } from "@nestjs/mongoose";
import { UserCreateDto } from "./dto/userCreateDto";
import { ClientProxy } from "@nestjs/microservices";
import { lastValueFrom } from "rxjs";
import { HttpService } from "@nestjs/axios";
import { AxiosResponse } from "axios";
import { NOTIFICATIONS_SERVICE, REQRES_SERVICE_BASE_URL } from "./constants/services";

@Injectable()
export class UsersService {
  constructor(
    @InjectModel(User.name) private userModel: Model<User>,
    @Inject(NOTIFICATIONS_SERVICE) private notificationsClient: ClientProxy,
    private readonly httpService: HttpService,
  ) {}

  async findAll(): Promise<User[]> {
    return this.userModel.find();
  }

  async create(user: UserCreateDto): Promise<User> {
    const createdUser: User = await this.userModel.create(user);
    await lastValueFrom((
      this.notificationsClient.emit('user_created.email', {'email': user.email})
    ))
    return createdUser;
  }

  async getUserById(id: string): Promise<AxiosResponse> {
    const url: string = `${REQRES_SERVICE_BASE_URL}/api/users/${id}`
    return this.httpService.axiosRef.get(url);
  }

  async deleteById(id: string): Promise<User> {
    return this.userModel.findByIdAndDelete(id);
  }
}
