import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { AbstractDocument } from "@app/common";


@Schema({
  timestamps: true
})
export class User extends AbstractDocument {
  @Prop({required: true})
  email: string;

  @Prop({required: true})
  first_name: string;

  @Prop({required: true})
  last_name: string;
}


export const UserSchema = SchemaFactory.createForClass(User);
