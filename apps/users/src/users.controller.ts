import {
  Body,
  Controller,
  Delete,
  Get,
  HttpException,
  NotFoundException,
  Param,
  Post,
  UsePipes,
  ValidationPipe
} from '@nestjs/common';
import { UsersService } from './users.service';
import { User } from "./schemas/users.schema";
import { UserCreateDto } from "./dto/userCreateDto";
import { ParseObjectIdPipe } from "@app/common";
import { AxiosResponse } from "axios";


@Controller('users')
export class UsersController {
  constructor(private userService: UsersService) {}

  @Get()
  async getUsers(): Promise<User[]> {
    /** TODO: need to write pagination **/
    return this.userService.findAll()
  }

  @Get(':id')
  async getUser(
    @Param('id') id: string
  ): Promise<object> {
    const response: AxiosResponse<any, any> | any = await this.userService.getUserById(id).catch(function (error) {
      if (error) {
        if (error.response.status == 404) {
          throw new NotFoundException('User not found');
        }
        throw new HttpException(error.response.data, error.response.status);
      }
    });
    return response.data;
  }

  @Delete(':id')
  async deleteUser(
    @Param('id', ParseObjectIdPipe) id: string
  ): Promise<User> {
    const user: User = await this.userService.deleteById(id);

    if (!user) {
      throw new NotFoundException('User not found');
    }

    return user;
  }

  @UsePipes(new ValidationPipe())
  @Post()
  async createUser(
    @Body() user: UserCreateDto
  ): Promise<User> {
    return this.userService.create(user);
  }
}
