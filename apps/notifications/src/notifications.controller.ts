import { Controller, Get } from '@nestjs/common';
import { NotificationsService } from './notifications.service';
import { Ctx, EventPattern, Payload, RmqContext } from "@nestjs/microservices";
import { RmqService } from "@app/common";

@Controller()
export class NotificationsController {
  constructor(private readonly notificationsService: NotificationsService, private readonly rmqService: RmqService) {}

  @EventPattern('user_created.email')
  async handleUserCreatedEmail(@Payload() data: any, @Ctx() context: RmqContext) {
    this.notificationsService.send_email(data);
    this.rmqService.ack(context);
  }
}
