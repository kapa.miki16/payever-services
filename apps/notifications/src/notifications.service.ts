import {Injectable, Logger} from '@nestjs/common';

@Injectable()
export class NotificationsService {
  private readonly logger = new Logger(NotificationsService.name);

  send_email(data: any) {
    this.logger.log('Notification sent', data)
  }
}
