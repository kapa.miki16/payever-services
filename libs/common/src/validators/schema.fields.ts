import { Types } from 'mongoose';
import { NotFoundException } from '@nestjs/common';
import { PipeTransform, Injectable } from '@nestjs/common';


function isValidId(id): boolean {
  return Types.ObjectId.isValid(id);
}


@Injectable()
export class ParseObjectIdPipe implements PipeTransform<any, Types.ObjectId> {
  /**
   * Checks the validity of the id field so that when getting from mongo by invalid id, the id does not throw an error
   *
   * @param value - The id
   * @returns The id or throw NotFoundException if not valid
   */
  public transform(value: any): Types.ObjectId {
    if (!isValidId(value)) {
      throw new NotFoundException('User not found');
    }
    return value
  }
}
